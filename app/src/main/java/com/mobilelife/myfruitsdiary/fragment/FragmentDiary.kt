package com.mobilelife.myfruitsdiary.fragment

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobilelife.myfruitsdiary.Api.MyFruitAPI
import com.mobilelife.myfruitsdiary.Api.Request.AddEntryReq
import com.mobilelife.myfruitsdiary.Api.Response.GetEntriesResp
import com.mobilelife.myfruitsdiary.Api.Response.GetFruitResp

import com.mobilelife.myfruitsdiary.R
import com.mobilelife.myfruitsdiary.adapter.EntriesAdapter
import com.mobilelife.myfruitsdiary.adapter.FruitsAdapter
import com.mobilelife.myfruitsdiary.classHelper.EntryClass
import com.mobilelife.myfruitsdiary.classHelper.FruitsClass
import com.mobilelife.myfruitsdiary.helper.DateUtils.getCurrentDateEntry
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_diary.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class FragmentDiary : Fragment(),EntriesAdapter.entriesInterface,FruitsAdapter.fruitsInterface {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_diary, container, false)
    }

    private lateinit var mContext: Context
    private lateinit var entriesAdapter: EntriesAdapter
    private var listOfEntries: MutableList<EntryClass> = mutableListOf()
    private var disposable: Disposable? = null
    private var currentFruitList: MutableList<FruitsClass> = mutableListOf()

    val myFruitApi by lazy {
        MyFruitAPI.createWithBasicAuth()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        entriesAdapter = EntriesAdapter(mContext, listOfEntries,this)
        rvEntries.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        rvEntries.addItemDecoration(DividerItemDecoration(rvEntries.context, DividerItemDecoration.VERTICAL))
        rvEntries.adapter = entriesAdapter

        fbAddEntry.setOnClickListener {
            val currentDate = getCurrentDateEntry()
            myFruitApi.addEntry(AddEntryReq(currentDate).getRequestBody()).enqueue(object :
                Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                    Timber.e("onFailure from postConfirm ")
                }

                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                    if(response.isSuccessful){
                        getAllEntries()
                    }else{
                        val errorText = response.errorBody()?.byteString()
                        Timber.e("onFailure from ${errorText} ")
                        Toast.makeText(mContext,errorText.toString(),Toast.LENGTH_LONG).show()
                    }
                }
            })
        }

        getAllEntries()
    }

    fun getAllEntries(){
        disposable = myFruitApi.getEntries()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onSuccess: MutableList<GetEntriesResp>? ->
                    onSuccess?.let { entries->
                        //lets get the fruits
                        disposable  = myFruitApi.getFruits()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                { onSuccess: MutableList<GetFruitResp>? ->
                                    onSuccess?.let { fruits->
                                        listOfEntries.clear()
                                        currentFruitList.clear()

                                        fruits.forEach {
                                            currentFruitList.add(FruitsClass(
                                                fruitId = it.id,
                                                fruitType = it.type,
                                                vitamins = it.vitamins,
                                                images = it.image
                                            ))
                                        }

                                        //we clear all current entries
                                        entries.forEach { currentEntry ->
                                            val currentEntryFruitList = mutableListOf<FruitsClass>()
                                            var totalVitamins = 0
                                            currentEntry.fruit.forEach { currentFruit->
                                                val vitamins = fruits.find { it.id == currentFruit.fruitId }?.vitamins ?: 0
                                                val images = fruits.find { it.id == currentFruit.fruitId }?.image ?: ""
                                                val a = FruitsClass(
                                                    fruitId = currentFruit.fruitId,
                                                    amount = currentFruit.amount,
                                                    fruitType = currentFruit.fruitType,
                                                    vitamins = vitamins,
                                                    images = images
                                                )
                                                totalVitamins += vitamins
                                                currentEntryFruitList.add(a)
                                            }
                                            listOfEntries.add(EntryClass(
                                                id = currentEntry.id,
                                                date = currentEntry.date,
                                                fruits = currentEntryFruitList,
                                                totalVitamins = totalVitamins
                                            ))
                                        }
                                        entriesAdapter.notifyDataSetChanged()
                                    }
                                },
                                { onError: Throwable ->

                                }
                            )
                    }
                },
                { onError: Throwable ->

                }
            )
    }

    override fun onLongClickEntry(entryId: Int,position:Int) {
        myFruitApi.deleteSpecificEntry(entryId).enqueue(object :
            Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                Timber.e("onFailure from deleteSpecificEntry ")
            }

            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                if(response.isSuccessful){
                    listOfEntries.removeAt(position)
                    entriesAdapter.notifyDataSetChanged()
                }else{

                    Timber.e("onFailure from deleteSpecificEntry ${response.body()} ")
                }
            }
        })
    }

    override fun onClickEntry(entryId: Int,position: Int) {
        val dialog = Dialog(mContext)
        dialog.setContentView(R.layout.layout_fruits_list)
        if (dialog.window != null) {
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) // this is optional
        }

        val rvFruitList: RecyclerView = dialog.findViewById(R.id.rvFruitList)
        rvFruitList.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        rvFruitList.addItemDecoration(DividerItemDecoration(rvEntries.context, DividerItemDecoration.VERTICAL))
        val currentEntryFruits = listOfEntries.find { it.getEntryId() == entryId }?.getFruitsCurrentEntry()
        val currentFruitOld = currentFruitList
        val fruitAdapter = FruitsAdapter(mContext,currentFruitOld,currentEntryFruits,entryId,this)
        rvFruitList.adapter = fruitAdapter

        val btnSave: Button = dialog.findViewById(R.id.btnSave)
        btnSave.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onChanges(entryId: Int, getCurrentCount: Int, fruitId: Int) {
        myFruitApi.editOrAddFruits(entryId,fruitId,getCurrentCount).enqueue(object :
            Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                Timber.e("onFailure from deleteSpecificEntry ")
            }

            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                getAllEntries()
            }
        })
    }

}
