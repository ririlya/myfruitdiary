package com.mobilelife.myfruitsdiary.helper

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    fun getCurrentDateEntry(): String {
        val c = Calendar.getInstance()
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return dateFormat.format(c.time)
    }
}