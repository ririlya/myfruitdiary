package com.mobilelife.myfruitsdiary.classHelper

class FruitsClass( private var fruitId: Int = 0,
        private var fruitType: String = "",
        private var amount:Int = 0,
        private var vitamins: Int = 0,
        private var images: String = "")
{
        fun getFruitId(): Int{
                return fruitId
        }
        fun getImages():String{
            return images
        }

        fun getVitamins():Int{
                return vitamins
        }

        fun getFruitType():String{
                return fruitType
        }

        fun getAmount():Int{
                return amount
        }

        fun setAmount(amount:Int){
                this.amount = amount
        }

}