package com.mobilelife.myfruitsdiary.classHelper

class EntryClass(
    private var id: Int,
    private var date: String,
    private var fruits: MutableList<FruitsClass>,
    private var totalVitamins: Int
) {

    fun getEntryId():Int{
        return id
    }

    fun getDate():String{
        return date
    }

    fun getFruitsCurrentEntry():MutableList<FruitsClass>{
        return fruits
    }

    fun getTotalVitamins():Int{
        return totalVitamins
    }
}