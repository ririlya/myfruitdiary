package com.mobilelife.myfruitsdiary.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobilelife.myfruitsdiary.R
import com.mobilelife.myfruitsdiary.classHelper.FruitsClass
import com.mobilelife.myfruitsdiary.helper.Constant

class FruitsAdapter(private var mContext: Context,private var data:MutableList<FruitsClass>,
                    private var currentEntryFruits: MutableList<FruitsClass>?, private var entryId: Int,
                    private val mListener: fruitsInterface)
    :RecyclerView.Adapter<FruitsAdapter.ViewHolder>() {

    interface fruitsInterface {
        fun onChanges(entryId: Int, getCurrentCount: Int, fruitId: Int)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal val ivFruitImage: ImageView = itemView.findViewById(R.id.ivFruitImage)
        internal val tvFruitName: TextView = itemView.findViewById(R.id.tvFruitName)
        internal val tvVitamin: TextView = itemView.findViewById(R.id.tvVitamin)
        internal val tvCountFruit: TextView = itemView.findViewById(R.id.tvCountFruit)
        internal val btnAdd: Button = itemView.findViewById(R.id.btnAdd)
        internal val btnMinus: Button = itemView.findViewById(R.id.btnMinus)
        internal val llFruit: LinearLayout = itemView.findViewById(R.id.llFruit)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_fruits_add_edit, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentData = data[position]

        Glide.with(mContext).load(Constant.baseUrl.plus("/"+currentData.getImages())).into(holder.ivFruitImage)
        holder.tvVitamin.text = mContext.resources.getString(R.string.no_of_vitamins,currentData.getVitamins().toString())
        holder.tvFruitName.text = currentData.getFruitType()

        val check = currentEntryFruits?.find { it.getFruitId() == currentData.getFruitId() }
        holder.tvCountFruit.text = when(check != null){
            true-> check.getAmount().toString()
            else-> "0"
        }

        holder.btnAdd.setOnClickListener {
            var getCurrentCount = (holder.tvCountFruit.text as String).toInt()
            getCurrentCount ++
            holder.tvCountFruit.text = getCurrentCount.toString()
            mListener.onChanges(entryId,getCurrentCount,currentData.getFruitId())
        }

        holder.btnMinus.setOnClickListener {
            var getCurrentCount = (holder.tvCountFruit.text as String).toInt()
            if(getCurrentCount > 0){
                getCurrentCount --
                holder.tvCountFruit.text = getCurrentCount.toString()
                mListener.onChanges(entryId,getCurrentCount,currentData.getFruitId())
            }
        }

    }


}