package com.mobilelife.myfruitsdiary.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mobilelife.myfruitsdiary.R
import com.mobilelife.myfruitsdiary.classHelper.EntryClass

class EntriesAdapter(private var mContext: Context,private var data: MutableList<EntryClass>,private var mListener: entriesInterface)
    : RecyclerView.Adapter<EntriesAdapter.ViewHolder>() {

    interface entriesInterface{
        fun onLongClickEntry(entryId: Int,position:Int)
        fun onClickEntry(entryId: Int,position:Int)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal val tvNoOfFruits: TextView = itemView.findViewById(R.id.tvNoOfFruits)
        internal val tvTotalVitamins: TextView = itemView.findViewById(R.id.tvTotalVitamins)
        internal val tvDateEntries: TextView = itemView.findViewById(R.id.tvDateEntries)
        internal val llEntry: LinearLayout = itemView.findViewById(R.id.llEntry)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_entries_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var currentData = data[position]

        holder.tvTotalVitamins.text = mContext.resources.getString(R.string.total_number_of_vitamins,currentData.getTotalVitamins().toString())
        holder.tvDateEntries.text = currentData.getDate()
        holder.tvNoOfFruits.text = mContext.resources.getString(R.string.total_number_of_fruits,currentData.getFruitsCurrentEntry().size.toString())
        holder.llEntry.setOnLongClickListener {view->
            val popupMenu = PopupMenu(mContext, view)
            popupMenu.menuInflater.inflate(R.menu.popup_menu_entry, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_delete_entry -> {
                        mListener.onLongClickEntry(currentData.getEntryId(),position)
                    }
                }
                true
            }
            popupMenu.show()
            true
        }

        holder.llEntry.setOnClickListener {
            mListener.onClickEntry(currentData.getEntryId(),position)
        }
    }
}