package com.mobilelife.myfruitsdiary.Api.Response

data class GetEntriesResp(val id:Int,val date:String, val fruit: MutableList<FruitAmountResp>)
//{
//    "id": 1812,
//    "date": "2021-01-10",
//    "fruit": [
//    {
//        "fruitId": 1,
//        "fruitType": "apple",
//        "amount": 5
//    },
//    {
//        "fruitId": 6,
//        "fruitType": "pear",
//        "amount": 2
//    }
//    ]
//},