package com.mobilelife.myfruitsdiary.Api.Request

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class AddEntryReq(private val date: String) {

    fun getRequestBody(): RequestBody {
        val jsonObject = JSONObject()
        jsonObject.put("date", date)
        return jsonObject.toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
    }
}