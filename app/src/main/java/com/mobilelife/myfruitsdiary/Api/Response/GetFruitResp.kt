package com.mobilelife.myfruitsdiary.Api.Response

data class GetFruitResp(val id:Int,val type:String,val vitamins: Int,val image: String)

// "id": 2,
//    "type": "banana",
//    "vitamins": 11,
//    "image": "images/banana.png"