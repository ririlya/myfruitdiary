package com.mobilelife.myfruitsdiary.Api

import android.content.SharedPreferences
import com.mobilelife.myfruitsdiary.Api.Response.GetEntriesResp
import com.mobilelife.myfruitsdiary.Api.Response.GetFruitResp
import com.mobilelife.myfruitsdiary.helper.Constant
import io.reactivex.Observable
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface MyFruitAPI {
    @GET("/api/fruit")
    fun getFruits(): Observable<MutableList<GetFruitResp>>

    @GET("/api/entries")
    fun getEntries():Observable<MutableList<GetEntriesResp>>

    @DELETE("/api/entries")
    fun deleteAllEntries():Observable<Void>

    @DELETE("/api/entry/{entryId}")
    fun deleteSpecificEntry(@Path("entryId") entryId: Int): Call<ResponseBody>

    @POST("/api/entries")
    fun addEntry(@Body requestBody: RequestBody): Call<ResponseBody>

    @POST("/api/entry/{entryId}/fruit/{fruitId}")
    fun editOrAddFruits(
        @Path("entryId") entryId: Int,
        @Path("fruitId") fruitId: Int,
        @Query("amount") amount: Int
    ):Call<ResponseBody>

    companion object {
        fun createWithBasicAuth(): MyFruitAPI {

            val clientBuilder = OkHttpClient.Builder()
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            clientBuilder
                .addInterceptor(logging)

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder.build())
                .baseUrl(Constant.baseUrl)
                .build()

            return retrofit.create(MyFruitAPI::class.java)
        }

    }

}