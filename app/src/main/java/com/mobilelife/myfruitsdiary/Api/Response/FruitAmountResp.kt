package com.mobilelife.myfruitsdiary.Api.Response

data class FruitAmountResp(val fruitId: Int, val fruitType: String, val amount:Int)
//{
//        "fruitId": 6,
//        "fruitType": "pear",
//        "amount": 2
//      }